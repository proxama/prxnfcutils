/*
* Product: Proxama NFC Utilities
* Copyright: Proxama Ltd 2012. (c) All rights reserved
* @author Steve Engledow <steve.engledow@proxama.com>
*/

var nfc_utils = require("./index");

var ndef_string = "d10227537091010a5402656e50726f78616d615101155500687474703a2f2f70726f78616d612e636f6d";

var text_record = {
    "message_begin": 1,
    "message_end": 0,
    "chunk_flag": 0,
    "short_record": 1,
    "id_present": 0,
    "tnf": "well-known type",
    "type_length": 1,
    "payload_length": 10,
    "type": "T",
    "payload": {
        "utf16": 0,
        "RFU": 0,
        "language_code_length": 2,
        "language_code": "en",
        "text": "Proxama"
    }
};

var url_record = {
    "message_begin": 0,
    "message_end": 1,
    "chunk_flag": 0,
    "short_record": 1,
    "id_present": 0,
    "tnf": "well-known type",
    "type_length": 1,
    "payload_length": 21,
    "type": "U",
    "payload": {
        "identifier": "",
        "uri": "http://proxama.com"
    }
};

var smart_poster_record = {
    "message_begin": 1,
    "message_end": 1,
    "chunk_flag": 0,
    "short_record": 1,
    "id_present": 0,
    "tnf": "well-known type",
    "type_length": 2,
    "payload_length": 39,
    "type": "Sp",
    "payload": [text_record, url_record]
};

// Check we can generate NDEF
console.log("Generated Text record:", nfc_utils.generator.generate_ndef(text_record));
console.log("Generated URL record:", nfc_utils.generator.generate_ndef(url_record));
console.log("Generated Smart Poster record:", nfc_utils.generator.generate_ndef(smart_poster_record));

// Check we can parse NDEF
console.log("Parsed NDEF as:", JSON.stringify(nfc_utils.parser.parse_ndef(ndef_string)));

// Check that generating a string from the output of the parser results in the original string
console.log("Parser and Generator agree:", ndef_string === nfc_utils.generator.generate_ndef(nfc_utils.parser.parse_ndef(ndef_string)));
