/*
* Product: Proxama NFC Utilities
* Copyright: Proxama Ltd 2012. (c) All rights reserved
* @author Steve Engledow <steve.engledow@proxama.com>
*/

function rebase(num, fromBase, toBase, places) {
    var out = parseInt(num, fromBase);

    out = out.toString(toBase);

    if(!isNaN(places)) {
        while(out.length < places) {
            out = "0" + out;
        }
    }

    return out;
}

function hexToString(hex) {
    var out = "";

    for(var i = 0; i<hex.length; i+= 2) {
        out += String.fromCharCode(rebase(hex.substring(i, i+2), 16, 10));
    }

    return out;
}

function hexFromString(string) {
    var out = "";

    for(var i=0; i<string.length; i++) {
        out += rebase(string.charCodeAt(i), 10, 16, 2);
    }

    return out;
}

function splitHex(hex) {
    var out = [];

    for(var i=0; i<hex.length; i+= 2) {
        out.push(hex.substring(i, i+2));
    }

    return out;
}

function minimiseArray(array) {
    if(array.length == 0) {
        return "";
    } else if(array.length == 1) {
        return array[0];
    }
    return array;
}

exports.rebase = rebase;
exports.hexToString = hexToString;
exports.hexFromString = hexFromString
exports.splitHex = splitHex;
exports.minimiseArray = minimiseArray;
