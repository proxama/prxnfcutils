Product: Proxama NFC Utilities

Copyright: Proxama Ltd 2012. (c) All rights reserved

@author Steve Engledow <steve.engledow@proxama.com>

---

# About

Proxama NFC Utilities is a collection of functions that generate and parse NFC-related data formats to and from JSON.

The library was designed to run in [node](http://nodejs.org) but could be easily adapted to other environments.

## Example Usage

### Parsing

    var nfcutils = require("path/to/nfcutils");

    var ndef = "91010a5402656e50726f78616d61";

    var message = nfcutils.parser.parse_ndef(ndef);

The variable `message` now contains:

    {
        message_begin: 1,
        message_end: 0,
        chunk_flag: 0,
        short_record: 1,
        id_present: 0,
        tnf: 'well-known type',
        type_length: 1,
        payload_length: 10,
        type: 'T',
        payload: {
            utf16: 0,
            RFU: 0,
            language_code_length: 2,
            language_code: 'en',
            text: 'Proxama'
        }
    }

The `payload` property will contain the data portion of the NDEF message, The other properties are various flags which are explained in the NDEF message documentation available from [NXP](http://nxp.com).

### Generating

    var nfcutils = require("path/to/nfcutils");

    var message = {
        message_begin: 1,
        message_end: 1,
        chunk_flag: 0,
        short_record: 1,
        id_present: 0,
        tnf: 'well-known type',
        type_length: 1,
        payload_length: 10,
        type: 'T',
        payload: {
            utf16: 0,
            RFU: 0,
            language_code_length: 2,
            language_code: 'en',
            text: 'Hello, world!'
        }
    };

    var ndef = nfcutils.generator.generate_ndef(message);

The `ndef` variable now contains: `d1010a5402656e48656c6c6f2c20776f726c6421`
