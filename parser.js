/*
* Product: Proxama NFC Utilities
* Copyright: Proxama Ltd 2012. (c) All rights reserved
* @author Steve Engledow <steve.engledow@proxama.com>
*/

var fs = require("fs");

var utils = require("./utils");

var ndef_dict = JSON.parse(fs.readFileSync("./ndefdict.json", "utf8"));

// Raw is an array of byte (or bit) values
function parse_value(raw, record, rule, base) {
    if(typeof(rule) == "string") {
        record[rule] = parseInt(utils.rebase(raw.join(""), base, 10));
    } else if(rule.name && typeof(rule.name.forEach) == "function") {
        // We've got more than one name, loop through each
        rule.name.forEach(function(name) {
            var new_rule = {};
            for(var prop in rule) {
                new_rule[prop] = rule[prop];
            }
            new_rule.name = name;
            parse_value(raw, record, new_rule, base);
        });
    } else {
        if(rule.bits) {
            // Create an array of bits and farm it off to parse_rules
            var bits = utils.rebase(raw.join(""), base, 2, raw.length * 8);
            bits = bits.split("");

            bits = parse_rules(bits, rule.bits, 2);

            bits.forEach(function(result) {
                for(var prop in result) {
                    record[prop] = result[prop];
                }
            });
        } else {
            if(!rule.name || typeof(rule.name) != "string") {
                throw "Rule must have a name: " + JSON.stringify(rule);
            }

            var matched = false;
            if(rule.values) {
                for(var val in rule.values) {
                    if(utils.rebase(val, base, 10) == utils.rebase(raw.join(""), base, 10)) {
                        record[rule.name] = rule.values[val];
                        matched = true;
                    }
                }
            }

            if(!matched) {
                var type="number";

                if(rule.format) {
                    // We're using an embedded type
                    var result = parse_rules(raw, rule.format);
                    record[rule.name] = utils.minimiseArray(result);
                } else {
                    if(rule.type) {
                        if(typeof(rule.type) == "string") {
                            type = rule.type;
                        } else if(typeof(rule.type.forEach) == "function") {
                            type = "";
                            rule.type.forEach(function(ref) {
                                if(typeof(record[ref]) != "undefined") {
                                    type += record[ref] + "/";
                                } else {
                                    throw "Could not parse type, missing variable: " + ref;
                                }
                            });
                            type = type.replace(/\/$/, "");
                        } else {
                            // We've embedded the type definition directly
                            throw "Could not parse type: " + JSON.stringify(rule.type);
                        }
                    }

                    if(type == null) {
                        // We skip this because we've already done it!
                    } else if(type == "string") {
                        record[rule.name] = utils.hexToString(raw.join(""));
                    } else if(type == "number") {
                        record[rule.name] = parseInt(utils.rebase(raw.join(""), base, 10));
                    } else if(type == "raw") {
                        record[rule.name] = raw.join("");
                    } else {
                        if(ndef_dict[type]) {
                            var result = parse_rules(raw, ndef_dict[type]);
                            record[rule.name] = utils.minimiseArray(result);
                        } else if(ndef_dict.types[type]) {
                            if(ndef_dict[ndef_dict.types[type]]) {
                                var result = parse_rules(raw, ndef_dict[ndef_dict.types[type]]);
                                record[rule.name] = utils.minimiseArray(result);
                            } else {
                                // We'll try to assume it's a native type and re-parse
                                var result = parse_rules(raw, [{name: "value", type: ndef_dict.types[type], length: -1}]);
                                record[rule.name] = utils.minimiseArray(result).value;
                            }
                        } else {
                            record[rule.name] = raw.join("");
                        }
                    }
                }
            }
        }
    }
}

// Raw is an array of byte (or bit) values
function parse_rules(raw, rules, base) {
    var pointer = 0;
    var data = [];

    if(!base) {
        base = 16;
    }

    while(pointer < raw.length) {
        var record = {};
        rules.forEach(function(rule) {
            // Find out if we need to skip
            var present = false;
            if(!rule.present_if) {
                present = true;
            } else {
                if(typeof(rule.present_if) == "string") {
                    if(!isNaN(record[rule.present_if])) {
                        present = record[rule.present_if];
                    } else {
                        present = !!record[rule.present_if];
                    }
                } else {
                    present = true; // Default to true and run away if we find anything that breaks

                    for(var prop in rule.present_if) {
                        if(typeof(rule.present_if[prop]) == "object") {
                            // Or...
                            present = false;
                            rule.present_if[prop].forEach(function(val) {
                                if(record[prop] === val) {
                                    present = true;
                                }
                            });
                        } else {
                            if(record[prop] !== rule.present_if[prop]) {
                                present = false;
                                break;
                            }
                        }
                    }
                }
            }

            if(pointer < raw.length && present) {
                var length = 1;
                if(typeof(rule) == "object" && rule.length) {
                    if(!isNaN(rule.length)) {
                        length = rule.length;
                    } else {
                        if(!isNaN(record[rule.length])) {
                            length = record[rule.length];
                        } else {
                            //throw "Could not parse length";
                        }
                    }
                }
                if(length == -1) {
                    length = raw.length - pointer;
                }

                var item = raw.slice(pointer, pointer+length);
                pointer += length;

                parse_value(item, record, rule, base);
            }
        });

        data.push(record);
    }

    return data;
}

function parse_ndef(ndef) {
    ndef = utils.splitHex(ndef);
    ndef = parse_rules(ndef, ndef_dict.ndef);

    return utils.minimiseArray(ndef);
}

exports.parse_ndef = parse_ndef;
