/*
* Product: Proxama NFC Utilities
* Copyright: Proxama Ltd 2012. (c) All rights reserved
* @author Steve Engledow <steve.engledow@proxama.com>
*/

exports.parser = require("./parser");
exports.generator = require("./generator");
