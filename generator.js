/*
* Product: Proxama NFC Utilities
* Copyright: Proxama Ltd 2012. (c) All rights reserved
* @author Steve Engledow <steve.engledow@proxama.com>
*/

var fs = require("fs");

var utils = require("./utils");

var ndef_dict = JSON.parse(fs.readFileSync("./ndefdict.json", "utf8"));

// Raw is an array of byte (or bit) values
function parse_value(record, rule, base) {
    var raw = "";
    var length = 1;

    if(typeof(rule) == "string") {
        if(typeof(record) == "undefined" || typeof(record) == "string") {
            record = {};
        }
        if(typeof(record[rule]) == "undefined") {
            record[rule] = 0;
        }
        raw = utils.rebase(record[rule], 10, base);
    } else if(rule.name && typeof(rule.name.forEach) == "function") {
        // We've got more than one name.
        // Run for just the first but then copy the values into the record for all others
        
        var found = false;

        rule.name.forEach(function(name) {
            if(!found && typeof(record[name]) != "undefined") {
                found = true;

                var new_rule = {};
                for(var prop in rule) {
                    new_rule[prop] = rule[prop];
                }
                new_rule.name = name;

                raw = parse_value(record, new_rule, base);

                rule.name.forEach(function(n) {
                    record[n] = record[name];
                });
            }
        });

    } else {
        if(rule.bits) {
            raw = utils.rebase(parse_rules(record, rule.bits, 2), 2, base);
        } else {
            if(rule.name && typeof(rule.name.forEach) == "function") {
                rule.name = rule.name[0];
            }

            if(typeof(record[rule.name]) == "undefined") {
                if(typeof(record) != "object") {
                    record = {};
                }
                record[rule.name] = "0";
            }

            var matched = false;
            if(rule.values) {
                for(var val in rule.values) {
                    if(record[rule.name] == rule.values[val]) {
                        raw = val;
                        matched = true;
                    }
                }
            }

            if(!matched) {
                var type="number";

                if(rule.format) {
                    // We're using an embedded type
                    raw = parse_rules(record[rule.name], rule.format);
                } else {
                    if(rule.type) {
                        if(typeof(rule.type) == "string") {
                            type = rule.type;
                        } else if(typeof(rule.type.forEach) == "function") {
                            type = "";
                            rule.type.forEach(function(ref) {
                                if(typeof(record[ref]) != "undefined") {
                                    type += record[ref] + "/";
                                } else {
                                    throw "Could not parse type, missing variable: " + ref;
                                }
                            });
                            type = type.replace(/\/$/, "");
                        } else {
                            throw "Could not parse type: " + JSON.stringify(rule.type);
                        }
                    }

                    if(type == null) {
                        // We skip this because we've already done it!
                    } else if(type == "string") {
                        raw = utils.hexFromString(record[rule.name]);
                    } else if(type == "number") {
                        raw = utils.rebase(record[rule.name], 10, base);
                    } else if(type == "raw") {
                        raw = record[rule.name];
                    } else {
                        if(ndef_dict[type]) {
                            raw = parse_rules(record[rule.name], ndef_dict[type]);
                        } else if(ndef_dict.types[type]) {
                            if(ndef_dict[ndef_dict.types[type]]) {
                                raw = parse_rules(record[rule.name], ndef_dict[ndef_dict.types[type]]);
                            } else {
                                // We'll try to assume it's a native type and re-parse
                                raw = parse_rules({value: record[rule.name]}, [{name: "value", type: ndef_dict.types[type], length: -1}]);
                            }
                        } else {
                            raw = record[rule.name];
                        }
                    }
                }
            }
        }

        if(rule.length) {
            if(typeof(rule.length) == "string") {
                length = raw.length;
                if(base == 16) {
                    length /= 2;
                }
                if(isNaN(record[rule.length])) {
                    record[rule.length] = length;
                }
            } else if(!isNaN(rule.length)) {
                length = rule.length;
            }
        }
    }

    if(base == 16) {
        length *= 2;
    }

    while(raw.length < length) {
        raw = "0" + raw;
    }

    return raw;
}

// Raw is an array of byte (or bit) values
function parse_rules(record, rules, base) {
    var raw = "";

    if(!base) {
        base = 16;
    }

    if(typeof(record) == "undefined") {
        record = {};
    }

    if(typeof(record.forEach) == "function") {
        // We've got an array, loop through it
        record.forEach(function(r) {
            raw += parse_rules(r, rules, base);
        });
    } else {
        for(var i=rules.length-1; i>=0; i--) {
            var rule = rules[i];

            // Find out if we need to skip 
            var present = false; 
            if(!rule.present_if) { 
                present = true;  
            } else {   
                if(typeof(rule.present_if) == "string") { 
                    if(!isNaN(record[rule.present_if])) { 
                        present = record[rule.present_if]; 
                    } else { 
                        present = !!record[rule.present_if]; 
                    }  
                } else { 
                    present = true; // Default to true and run away if we find anything that breaks 

                    for(var prop in rule.present_if) { 
                        if(typeof(rule.present_if[prop]) == "object") { 
                            // Or... 
                            present = false; 
                            rule.present_if[prop].forEach(function(val) { 
                                if(record[prop] === val) { 
                                    present = true; 
                                } 
                            }); 
                        } else { 
                            if(record[prop] !== rule.present_if[prop]) { 
                                present = false; 
                                break; 
                            } 
                        } 
                    } 
                } 
            }

            if(present) {
                var value = parse_value(record, rule, base);
                raw = value + raw;
            }
        }
    }

    return raw;
}

function generate_ndef(records) {
    ndef = parse_rules(records, ndef_dict.ndef);

    return ndef;
}

exports.generate_ndef = generate_ndef;

/*
console.log(generate_ndef([
    {
        "message_begin": 1,
        "message_end": 0,
        "chunk_flag": 0,
        "short_record": 1,
        "id_present": 0,
        "tnf": "external type",
        "type": "prx:hub",
        "payload": [
            {
                "key": "id",
                "value": "wDz"
            },
            {
                "key": "brand_code",
                "value": "\u0014"
            },
            {
                "key": "html",
                "value": "1234567890"
            },
            {
                "key": "short_desc",
                "value": "Orange Wednesdays 2 for 1"
            },
            {
                "key": "url",
                "value": "http://o.prx.ma/u86z"
            },
            {
                "key": "button",
                "value": "Get yours now"
            }
        ]
    },
    {
        "message_begin": 0,
        "message_end": 1,
        "chunk_flag": 0,
        "short_record": 0,
        "id_present": 0,
        "tnf": "well-known type",
        "type": "Sp",
        "payload": [
            {
                "message_begin": 1,
                "message_end": 0,
                "chunk_flag": 0,
                "short_record": 1,
                "id_present": 0,
                "tnf": "well-known type",
                "type": "T",
                "payload": {
                    "utf16": 0,
                    "RFU": 0,
                    "language_code": "en",
                    "text": "Get yours now"
                }
            },
            {
                "message_begin": 0,
                "message_end": 1,
                "chunk_flag": 0,
                "short_record": 1,
                "id_present": 0,
                "tnf": "well-known type",
                "type_length": 1,
                "type": "U",
                "payload": {
                    "identifier": "http://",
                    "uri": "o.prx.ma/u86z"
                }
            }
        ]
    }
]));
*/
